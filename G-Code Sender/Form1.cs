﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.IO;

namespace G_Code_Sender
{
    public partial class Form1 : Form
    {
        SerialPort _serialPort;
        int xpos = 0;
        int ypos = 0;

        int penpos = 100;
        int penmin = 100;
        int penmax = 140;
        int linenumber;
        public bool isCanceled { get; set; }
        public delegate void highlightCurrentLine(RichTextBox richTextBox, int index, Color color, Color foreground);
        public delegate void appendLog(RichTextBox richTextBox, string logline);
        public delegate void controlStatusLabel(RichTextBox inputbox, ToolStripStatusLabel status);
        public delegate void controlProgressbar(StatusStrip ss, ToolStripProgressBar pb, RichTextBox rtb, int linenumber);
        public delegate void controlSendButton(Button btn, Boolean flag);
        public delegate void controlTimeLable(ToolStripLabel lbl, String text);

        private string filepath;
        DateTime starttime;

        public Form1(String[] args)
        {
            InitializeComponent();
            timerConnected.Start();

            if (args.Length > 0) {
                filepath = args[0];
            }
        }
        private void connectToSerial() {
            if (cmbComport.SelectedItem != null)
            {
                if (_serialPort == null || !_serialPort.IsOpen)
                {
                    btnConnect.BackColor = Color.Yellow;
                    btnConnect.Text = "Connecting";
                    btnConnect.Refresh();
                    string comport = cmbComport.SelectedItem.ToString();
                    _serialPort = new SerialPort(comport, (int)cmbBaudrate.SelectedItem, Parity.None, 8, StopBits.One);
                    _serialPort.Handshake = Handshake.None;
                    _serialPort.Open();
                    System.Threading.Thread.Sleep(1000);
                    btnConnect.Text = "Disconnect";
                }else{
                    
                }
            }
            else
            {
                MessageBox.Show("No COM port selected");
            }
        }

        private void disconnectSerial() {
            if (_serialPort != null || _serialPort.IsOpen)
            {
                _serialPort.Close();
                btnConnect.BackColor = Color.Red;
                btnConnect.Text = "Connect";
            }
        }

        private void sendMessage(string text) {
            
            if (_serialPort == null || !_serialPort.IsOpen)
            {
                btnConnect.PerformClick();
            }

            _serialPort.WriteLine(text);
        }

        private void updateLog(RichTextBox rtb, string text) {
            rtbLog.AppendText(DateTime.Now.ToString("HH:mm:ss.ffff   ") + text + Environment.NewLine);
            rtbLog.SelectionStart = rtbLog.Text.Length;
            rtbLog.ScrollToCaret();
        }

        private void updateStatusLabel(RichTextBox inputbox, ToolStripStatusLabel status) {
            int cursorPosition = inputbox.SelectionStart;
            int lineIndex = inputbox.GetLineFromCharIndex(cursorPosition);
            status.Text = String.Format("processing line {0} from {1}", lineIndex + 1, inputbox.Lines.Count().ToString());
        }

        private void updateSendButton(Button btn, Boolean flag) {
            btn.Enabled = flag;
        }

        private void updateTimeLabel(ToolStripLabel lbl, String text)
        {
            lbl.Text = String.Format("ca. {0}", text);
        }

        private void updateProgressbar(StatusStrip ss, ToolStripProgressBar pb, RichTextBox rtb, int linenumber) {
            pb.Step = 1;
            pb.Minimum = 0;
            pb.Maximum = rtb.Lines.Count();
            pb.Value = linenumber+1;
            ss.Refresh();
        }
        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (_serialPort == null || !_serialPort.IsOpen) {
                connectToSerial();
            } else {
                disconnectSerial();
            }
        }
       
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string[] ports = SerialPort.GetPortNames();
                foreach (string port in ports)
                {
                    cmbComport.Items.Add(port);
                }
                cmbComport.SelectedIndex = cmbComport.Items.IndexOf(ports[0]);

                int[] baudrates = { 4800, 9600, 115200 };
                foreach (int baud in baudrates)
                {
                    cmbBaudrate.Items.Add(baud);
                }

                cmbBaudrate.SelectedIndex = cmbBaudrate.Items.IndexOf(baudrates[2]);
            }
            catch { }
            richTextBox1.Text = @"-- DEMO CODE --
G1 X-1285.52 Y1000 F2900.00
M300 S130.00
G1 X-1285.52 Y-1000 F2900.00
M300 S100.00
G1 X-1285.52 Y0 F2900.00";

            if (filepath != null && filepath != "") {
                richTextBox1.Text = File.ReadAllText(filepath);
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            btnSend.Enabled = false;
            remainingTimer.Start();
            if (_serialPort == null || !_serialPort.IsOpen) {
                connectToSerial();
            }   
            //pbProgress.

            String[] lines = richTextBox1.Text.Split('\n');
            linenumber = 0;
            isCanceled = false;
            starttime = DateTime.Now;
            
            Thread t = new Thread(() =>
            {
                foreach (var line in lines)
                {
                    if (isCanceled)
                    {
                        try
                        {
                            btnSend.Invoke(new controlSendButton(updateSendButton), btnSend, true);
                        }
                        catch { }
                        break;
                    }
                            
                    try
                    {
                        _serialPort.WriteLine(line);
                        richTextBox1.Invoke(new highlightCurrentLine(updatertb), richTextBox1, linenumber, Color.LightBlue, Color.White);
                        sendMessage(line);
                        rtbLog.Invoke(new appendLog(updateLog), rtbLog, line);
                        rtbLog.Invoke(new controlStatusLabel(updateStatusLabel), richTextBox1, lblStatus);
                        rtbLog.Invoke(new controlProgressbar(updateProgressbar), statusStrip1, pbProgress, richTextBox1, linenumber);

                        linenumber += 1;
                    }
                    catch(Exception ex) {
                        
                    }
                }
            rtbLog.Invoke(new appendLog(updateLog), rtbLog, "");
            rtbLog.Invoke(new appendLog(updateLog), rtbLog, String.Format("Work is done in: {0} for {1} lines.", (DateTime.Now - starttime).ToString(), linenumber));
        });
            t.Start();
            btnSend.Enabled = true;
        }

        private void timerConnected_Tick(object sender, EventArgs e)
        {
            if (_serialPort != null && _serialPort.IsOpen)
            {
                btnConnect.BackColor = Color.Green;
                btnConnect.Text = "Disconnect";
            }
            else {
                btnConnect.BackColor = Color.Red;
                btnConnect.Text = "Connect";
            }
        }
        public void updatertb(RichTextBox richTextBox, int index, Color color, Color foreground)
        {
            richTextBox.SelectAll();
            richTextBox.SelectionBackColor = richTextBox.BackColor;
            var lines = richTextBox.Lines;
            if (index < 0 || index >= lines.Length)
                return;
            var start = richTextBox.GetFirstCharIndexFromLine(index);
            var length = lines[index].Length;
            richTextBox.Select(start, length);
            richTextBox.SelectionBackColor = color;
            richTextBox.Refresh();
            richTextBox.ScrollToCaret();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = @"G1 X-1285.52 Y1000 F2900.00
M300 S130.00
G1 X-1285.52 Y-1000 F2900.00
M300 S100.00
G1 X-1285.52 Y0 F2900.00";
            btnSend.PerformClick();
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            if (penpos > penmin)
            {
                penpos -= 2;
                btnDown.Enabled = true;
            }
            else {
                btnUp.Enabled = false;
            }
            
            sendMessage(String.Format("M300 S{0}", penpos));
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            if (penpos < penmax)
            {
                penpos += 2;
                btnUp.Enabled = true;
            }
            else
            {
                btnDown.Enabled = false;
            }
            
            sendMessage(String.Format("M300 S{0}", penpos));
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            ypos += 10;
            sendMessage(String.Format("G1 X{0} Y{1} F1000", xpos, ypos));
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            ypos -= 10;
            sendMessage(String.Format("G1 X{0} Y{1} F1000", xpos, ypos));
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            btnSend.Enabled = true;
            remainingTimer.Stop();
            isCanceled = true;
            richTextBox1.Invoke(new highlightCurrentLine(updatertb), richTextBox1, 1, Color.White, Color.Black);
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            sendMessage(String.Format("M300 S100", xpos, ypos));
            sendMessage(String.Format("G0 X0 Y0", xpos, ypos));
        }
        private void remainingTimer_Tick(object sender, EventArgs e)
        {
            int commands = richTextBox1.Lines.Count();
            double timePerLine = (DateTime.Now - starttime).Seconds / ((linenumber + 1) * 1.0000);
            double secondsRemaining = (commands - linenumber) * timePerLine;
            TimeSpan remaining = TimeSpan.FromSeconds((int)secondsRemaining);
            rtbLog.Invoke(new controlTimeLable(updateTimeLabel), lblTime, remaining.ToString());
        }

        private void Form1_FormClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            btnStop.PerformClick();

            //e.Cancel = false;
        }
    }
}

